const express = require('express');
const app = express();
const cors = require('cors')
app.use(cors())
app.use(express.json())
const MongoClient = require("mongodb").MongoClient;
const mongourl = "mongodb+srv://cca-duppalav1:duppalav1@cca-duppalav1.kzzvn.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err=>{
    if(err) throw err;
    console.log("Connected to the MongoDB Cluster");
})
var port =  process.env.PORT || 8080;

app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port : ${port}`))
    
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/cca-form.html');
})  

let fields = {
    _id : false,
    zips: true,
    city: true,
    state_id: true,
    state_name: true,
    county_name: true,
    timezone: true
};

app.get('/uscities-search', (req, res) => {
    res.send("US City Search by Vamsi Duppala");
})  

app.get('/uscities-search/:zips(\\d{1,5})', (req, res) => {
    const db = dbClient.db();
    // res.send(req.url + req.params.zips);
    let zipRegEx = new RegExp(parseInt(req.params.zips));
    const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    })
})  

app.get('/uscities-search/:city', (req, res) => {
    const db = dbClient.db();
    // res.send(req.url + req.params.zips);
    let cityRegEx = new RegExp(req.params.city, "i");
    const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    })
})
  

app.get('/echo.php', function(req, res){
    var data = req.query.data
    res.send(data)
})
app.post('/echo.php', function(req, res){
    var data = req.body['data']
    res.send(data)
})

app.post('/signup', function(req, res){
    const {username, password, fullname, email} = req.body;
    const db = dbClient.db();
    (async () => {
        console.log("ALERT : got a sign up request!")
        try{
            var userLength = await db.collection("users").countDocuments({email:email});
            if(userLength > 0){
                res.send({"userLength" : userLength, "message" : "Email already Exists"});
            }
            else{
                await db.collection("users").insertOne({email:email, password : password, fullname : fullname, username : username});
                res.send({"userLength" : 1, "message": "Signed up Successfully! Please Log in now."})
            }
        }
        catch(err){
            console.log(err)
        }

    })();
})


app.post('/login', function(req, res){
    const {username, password, fullname, email} = req.body;
    const db = dbClient.db();

    (async () => {
        try{
            var userCount = await db.collection("users").countDocuments({email:email, password : password});
            console.log(await db.collection("users").findOne({email:email, password : password}))
            if(userCount>0){
                res.send({status: 0, logged:true, message : "login successful"})            
            }else{
                res.send({status: 1, logged:false, message : "login failed"})            
            }
        }
        catch(err){
            console.log(err)
            res.send({status: 2, logged:false, message : "login failed"})            

        }

    })();
})